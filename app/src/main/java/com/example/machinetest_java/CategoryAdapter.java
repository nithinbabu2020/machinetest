package com.example.machinetest_java;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.machinetest_java.DAO.Category;
import com.example.machinetest_java.DAO.Product;

import java.util.ArrayList;
import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {
    Context ctx;
    List<Category> catagory1;
    List<Product> product;
    private static int currentPosition = 0;

    public CategoryAdapter(Context applicationContext, List<Category> category1, List<Product> product1) {
        product = new ArrayList<>();
        this.ctx = applicationContext;
        this.catagory1 = category1;
        this.product = product1;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_layout, viewGroup, false);
        return new MyViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.textViewName.setText(catagory1.get(position).getTitle());
        //if the position is equals to the item position which is to be expanded
        if (currentPosition == position) {
            //creating an animation
            Animation slideDown = AnimationUtils.loadAnimation(ctx, R.anim.slide);
            //toggling visibility
            holder.linearLayout.setVisibility(View.VISIBLE);
            innerAdapter ia = new innerAdapter(ctx, product);
            holder.innerrecycle.setAdapter(ia);
            //adding sliding effect
            holder.linearLayout.startAnimation(slideDown);
        }

        holder.textViewName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //getting the position of the item to expand it
                currentPosition = holder.getAdapterPosition();
                //reloding the list
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return catagory1.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textViewName;
        LinearLayout linearLayout;
        RecyclerView innerrecycle;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.textViewName);
            innerrecycle = itemView.findViewById(R.id.inner);
            GridLayoutManager ll = new GridLayoutManager(ctx, 2,GridLayoutManager.HORIZONTAL,false);
            innerrecycle.setLayoutManager(ll);
            linearLayout = itemView.findViewById(R.id.linearLayout);
        }
    }
}


