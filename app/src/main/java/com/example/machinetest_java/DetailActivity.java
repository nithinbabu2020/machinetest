package com.example.machinetest_java;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class DetailActivity extends AppCompatActivity {
TextView name,price,desc;
ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        name=findViewById(R.id.product);
        price=findViewById(R.id.price);
        desc=findViewById(R.id.desc);
        img=findViewById(R.id.imageView2);
        Bundle b=getIntent().getExtras();
        name.setText(b.getString("name"));
        price.setText(b.getString("price"));
        desc.setText(b.getString("desc"));
        Glide.with(getApplicationContext()).load(b.getString("img")).into(img);
    }
}