package com.example.machinetest_java;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.machinetest_java.DAO.Category;
import com.example.machinetest_java.DAO.Dao;
import com.example.machinetest_java.DAO.Product;
import com.example.machinetest_java.Network.ApiClient;
import com.example.machinetest_java.Network.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    RecyclerView cat;
    List<Category> category1;
    List<Product> product1;
    CategoryAdapter ca;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        cat = findViewById(R.id.erecycle);
        LinearLayoutManager ll = new LinearLayoutManager(this);
        cat.setLayoutManager(ll);
        category1 = new ArrayList<>();
        product1 = new ArrayList<>();
        getApi();
    }

    private void getApi() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<Dao> call = apiService.getdatafromApi();
        call.enqueue(new Callback<Dao>() {
            @Override
            public void onResponse(Call<Dao> call, Response<Dao> response) {
                category1 = response.body().getCategories();
                for (int i = 0; i < category1.size(); i++) {
                    product1 = category1.get(i).getProducts();
                }
                ca = new CategoryAdapter(getApplicationContext(), category1, product1);
                cat.setAdapter(ca);
            }

            @Override
            public void onFailure(Call<Dao> call, Throwable t) {

            }
        });
    }
}