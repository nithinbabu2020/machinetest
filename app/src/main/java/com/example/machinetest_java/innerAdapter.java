package com.example.machinetest_java;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.machinetest_java.DAO.Product;

import java.util.ArrayList;
import java.util.List;

public class innerAdapter extends RecyclerView.Adapter<innerAdapter.MyViewHolder> {
    Context ctx;
    List<Product> product;
    private static int currentPosition = 0;

    public innerAdapter(Context applicationContext, List<Product> product1) {
        product = new ArrayList<>();
        this.ctx = applicationContext;
        this.product = product1;
    }

    @NonNull
    @Override
    public innerAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_inner, viewGroup, false);
        return new innerAdapter.MyViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull innerAdapter.MyViewHolder holder, @SuppressLint("RecyclerView") int position) {

        holder.name.setText(product.get(position).getTitle());
        holder.price.setText(product.get(position).getPrice() + "");
        // holder.desc.setText(product.get(position).getDescription());
        Glide.with(ctx).load(product.get(position).getImageUrl()).into(holder.imageView);
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("name", product.get(position).getTitle());
                b.putString("img", product.get(position).getImageUrl());
                b.putString("desc", product.get(position).getDescription());
                b.putString("price", product.get(position).getPrice() + "");
                Intent i=new Intent(ctx,DetailActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                                i.putExtras(b);
                ctx.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return product.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textViewName, name, price;
        ImageView imageView;
        LinearLayout linearLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.textViewName);
            name = itemView.findViewById(R.id.pname);
            price = itemView.findViewById(R.id.price);
            imageView = itemView.findViewById(R.id.imageView);
            linearLayout = itemView.findViewById(R.id.linearLayout);

        }
    }
}



